package com.nabenik.rest;


import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.eclipse.microprofile.metrics.annotation.Metered;

@RequestScoped
@Path("/hello")
@Produces("text/plain")
@Consumes("text/plain")
public class HelloEndpoint {

    @GET
    @Metered
    public String doHello() throws UnknownHostException {
        InetAddress inetAddress = InetAddress.getLocalHost();
        
        String result = "Java is everywhere https://www.youtube.com/watch?v=zg79C7XM1Xs \n";
        result += inetAddress.getHostAddress() + "-" + inetAddress.getHostName() + "\n";
        result += "mod latest 1";
        return result;
    }
}

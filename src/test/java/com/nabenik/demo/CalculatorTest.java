/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nabenik.demo;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tuxtor
 */
public class CalculatorTest {
    
    Calculator calc;
    @Before
    public void setUp() {
        calc = new Calculator();
    }
    
    @Test
    public void testSum(){
        int expected = 10;
        assertEquals(expected, calc.sum(5, 5));
    }
    
}
